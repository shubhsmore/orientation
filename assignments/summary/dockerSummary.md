Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 

Docker provides tooling and a platform to manage the lifecycle of your containers:
- Develop your application and its supporting components using containers.
- The container becomes the unit for distributing and testing your application.
- When you’re ready, deploy your application into your production environment, as a container or an orchestrated service. This    works the same whether your production environment is a local data center, a cloud provider, or a hybrid of the two.

BUILD
- Build an image from the Dockerfile in the current directory and tag the image
    `docker build -t myimage:1.0`
 
- List all images that are locally stored with the Docker Engine
    `docker image ls`
 
- Delete an image from the local image store
    `docker image rm alpine:3.4`

SHARE
- Pull an image from a registry
    `docker pull myimage:1.0`
- Retag a local image with a new image name and tag 
    `docker tag myimage:1.0 myrepo/myimage:2.0`
- Push an image to a registry
    `docker push myrepo/myimage:2.0`

Docker Management
All commands below are called as options to the base docker command. Run docker <command> --help for more information on a particular command.

- `app*` --> Docker Application
- `assemble*` --> Framework-aware builds (Docker Enterprise)
- `builder` --> Manage builds
- `cluster` --> Manage Docker clusters (Docker Enterprise)
- `config` --> Manage Docker configs
- `context` --> Manage contexts
- `engine` --> Manage the docker Engine
- `image` --> Manage images
- `network` --> Manage networks
- `node` --> Manage Swarm nodes
- `plugin` --> Manage plugins
- `registry*` --> Manage Docker registries
- `secret` --> Manage Docker secrets
- `service` --> Manage services
- `stack` --> Manage Docker stacks
- `swarm` --> Manage swarm
- `system` --> Manage Docker
- `template*` --> Quickly scaffold services (Docker Enterprise)
- `trust` --> Manage trust on Docker images
- `volume` --> Manage volumes

Run a container from the Alpine version 3.9 image, name the running container “web” and expose port 5000 externally,mapped to port 80 inside the container.
`docker container run --name web -p 5000:80 alpine:3.9`

Stop a running container through SIGTERM
`docker container stop web`

Stop a running container through SIGKILL
`docker container kill web`

List the networks
`docker network ls`